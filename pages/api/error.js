export default (req, res) => {
  res.statusCode = 500
  res.json({ name: 'Internal Server Error' })
}